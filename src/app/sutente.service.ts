import { Injectable } from '@angular/core';

import { MessageService } from './message.service';
import { AtoresService } from './atores.service';
import { Prescricaoporaviar } from './objetos/prescricaoporaviar';
import { HttpClient, HttpResponse, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Subject } from 'rxjs';
import { Observable } from 'rxjs/Observable';
import { Configuracao } from './configuracao';


@Injectable()
export class SutenteService {

  configuracao = new Configuracao();

  constructor(
    private messageService: MessageService,
    private atoresService: AtoresService,
    private http: HttpClient 
  ) { }

    //Obtem lista de prescricoes por aviar de um utente depois de uma data.
    public getListaPrescricoesDeUtenteDepoisDe(token: string, utente: string, data: string): Observable<Prescricaoporaviar[]> {
      
          
          ///utente/utente/prescricao/poraviar/?data=11-12-1917
          var caminho: string = this.configuracao.receitaurl + "/utente/" + utente + "/prescricao/poraviar/?data=" + data;
      
          this.messageService.add("S utente: getListaPrescricoesDeUtenteDepoisDe " + caminho);
      
          let headers: HttpHeaders = new HttpHeaders({ 'Authorization': token, "Content-Type": "application/json" });
      
          return this.http.get<Prescricaoporaviar[]>(caminho, { headers });
        }

}
