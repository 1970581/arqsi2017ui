import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LogarComponent }      from './logar/logar.component';
import { RegistarComponent }      from './registar/registar.component';
import { ListarreceitasComponent }      from './listarreceitas/listarreceitas.component';
import { CriarreceitaComponent } from './criarreceita/criarreceita.component';
import { VerreceitaComponent } from './verreceita/verreceita.component';
import { ListaranonimoComponent } from './listaranonimo/listaranonimo.component';
import { UtenteporaviarComponent } from './utenteporaviar/utenteporaviar.component';

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LogarComponent },
  { path: 'registar', component: RegistarComponent },
  { path: 'listaranonimo', component: ListaranonimoComponent },
  { path: 'listarreceitas', component: ListarreceitasComponent},
  { path: 'criarreceita', component: CriarreceitaComponent },
  { path: 'verreceita', component: VerreceitaComponent },
  { path: 'utenteporaviar', component: UtenteporaviarComponent }
];


@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]  
})
export class AppRoutingModule {}