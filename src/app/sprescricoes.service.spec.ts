import { TestBed, inject } from '@angular/core/testing';

import { SprescricoesService } from './sprescricoes.service';

describe('SprescricoesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SprescricoesService]
    });
  });

  it('should be created', inject([SprescricoesService], (service: SprescricoesService) => {
    expect(service).toBeTruthy();
  }));
});
