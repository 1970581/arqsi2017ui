import { Component, OnInit } from '@angular/core';
import { MessageService } from '../message.service';
import { AtoresService } from '../atores.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  t :boolean = false;

  constructor(private messageService: MessageService, public atoresService: AtoresService) { }

  logout(){
    this.atoresService.logout();
  }

  ngOnInit() {
  }

}
