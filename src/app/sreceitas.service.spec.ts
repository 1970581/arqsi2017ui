import { TestBed, inject } from '@angular/core/testing';
import { SreceitasService } from './sreceitas.service';

describe('SreceitasService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SreceitasService]
    });
  });

  it('should be created', inject([SreceitasService], (service: SreceitasService) => {
    expect(service).toBeTruthy();
  }));
});
