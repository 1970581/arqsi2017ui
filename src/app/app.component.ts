import { Component } from '@angular/core';
import { LogarComponent } from './logar/logar.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'UI Receitas';
}
