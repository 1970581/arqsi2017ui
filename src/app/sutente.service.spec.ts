import { TestBed, inject } from '@angular/core/testing';

import { SutenteService } from './sutente.service';

describe('SutenteService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SutenteService]
    });
  });

  it('should be created', inject([SutenteService], (service: SutenteService) => {
    expect(service).toBeTruthy();
  }));
});
