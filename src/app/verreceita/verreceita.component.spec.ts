import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerreceitaComponent } from './verreceita.component';

describe('VerreceitaComponent', () => {
  let component: VerreceitaComponent;
  let fixture: ComponentFixture<VerreceitaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerreceitaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerreceitaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
