import { Component, OnInit } from '@angular/core';
import { MessageService } from '../message.service';
import { AtoresService } from '../atores.service';
import { SreceitasService } from '../sreceitas.service';
import { Receita} from '../objetos/receita';
import { HttpClient, HttpResponse, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { SprescricoesService } from '../sprescricoes.service';
import { Prescricao} from '../objetos/prescricao';

@Component({
  selector: 'app-verreceita',
  templateUrl: './verreceita.component.html',
  styleUrls: ['./verreceita.component.css']
})
export class VerreceitaComponent implements OnInit {

  idReceitaAVer: string = "";

  estado : string = "";
  estadoPrescricao: string = "";

  
  receita: Receita;

  prescricao: Prescricao;

  nAviar:number = 0;
  nMedicamento: string = "";

  constructor(
    private messageService: MessageService, 
    public atoresService: AtoresService, 
    private sreceitasService: SreceitasService,
    private sprescricoesService: SprescricoesService
  ) { }

  ngOnInit() {
  }

  // buscar a sugestao de receita. Copia a sugestao para o campo que interessa.
  public verSugestao(){
    this.idReceitaAVer = this.sreceitasService.sugestaoReceita;
    this.buscarReceita();
  }

  // Buscar a receita.
  public buscarReceita(){
    
    this.receita = null;
    this.prescricao = null;
    var idEsperado : string = this.idReceitaAVer;
    this.estado = "Buscando receita: " + idEsperado;
    this.messageService.add("C: verReceita A buscar receita id: " + this.idReceitaAVer);

    //Observador.
    this.sreceitasService.buscarReceitaPorID( this.idReceitaAVer) .subscribe(
      receita => {
        if (receita.idReceita == idEsperado) {
          this.receita = receita;
          this.estado = "Encontramos a receita: " + receita.idReceita;
        }
        this.messageService.add("C: verReceita Observou isto: " + JSON.stringify(this.receita));
      }
      ,
      (err: HttpErrorResponse) => {
        if (err.error instanceof Error) {
          // A client-side or network error occurred. Handle it accordingly.
          this.messageService.add("An error occurred:" + err.error.message);
          this.estado = "Erro";
          
        } else {
          // The backend returned an unsuccessful response code.
          // The response body may contain clues as to what went wrong,
          var mem: string = `Backend returned code ${err.status}, body was: ${err.error}`;
          this.messageService.add(mem);
          this.estado = "Erro";
          try{        this.messageService.add(JSON.stringify(err));}
          catch(algo){this.messageService.add("Excepcao apanhada.");};
        }
      }
    );
  }


  //Busca detalhe da prescricao em questao.
  public buscarPrescricao(idReceita: string, idPrescricao: number){
    this.messageService.add("C: verReceita buscando prescricao: " + idReceita + "/" + idPrescricao);
    this.nAviar = 0;

    this.sprescricoesService.buscarPrescricaoPorID(idReceita,idPrescricao)
    .subscribe(
      prescricao => {
        if (prescricao.idPrescricao == idPrescricao) {
          this.prescricao = prescricao;
          this.estadoPrescricao = "Encontramos a prescricao: " + idReceita + "/" + idPrescricao;
        }
        this.messageService.add("C: verReceita (Prescricao) Observou isto: " + JSON.stringify(prescricao));
      }
      ,
      (err: HttpErrorResponse) => {
        if (err.error instanceof Error) {
          // A client-side or network error occurred. Handle it accordingly.
          this.messageService.add("An error occurred:" + err.error.message);
          this.estadoPrescricao = "Erro";
          
        } else {
          // The backend returned an unsuccessful response code.
          // The response body may contain clues as to what went wrong,
          var mem: string = `Backend returned code ${err.status}, body was: ${err.error}`;
          this.messageService.add(mem);
          this.estadoPrescricao = "Erro";
          try{        this.messageService.add(JSON.stringify(err));}
          catch(algo){this.messageService.add("Excepcao apanhada.");};
        }
      }
    );

  }

  public add1(){
    if(this.prescricao){
      try{
        var nMax:number = this.prescricao.quantidade - this.prescricao.aviados;
        if (this.nAviar < nMax)  this.nAviar++;
      }
      catch(erro){
        this.messageService.add("Erro em add1()");
      }
    } 
    
  }

  public rem1(){
    this.nAviar--;
    if(this.nAviar < 0) this.nAviar = 0;
  }

  public max(){
    if(this.prescricao){
      try{
        var nMax:number = this.prescricao.quantidade - this.prescricao.aviados;
        this.nAviar = nMax.valueOf();
      }
      catch(erro){
        this.messageService.add("Erro em max()");
      }
    }
  }

  //Aviar uma determinada prescricao.
  public aviar(quantidade :number, idPrescricao: number, nome:string){
    

    let idReceita: string = "";
    if (this.receita) idReceita = this.receita.idReceita;

    this.estadoPrescricao = "Aviando: " + idReceita + "/" + idPrescricao;

    //Debug:
    this.messageService.add("C: verReceita aviando: " + idReceita + "/" + idPrescricao + " qtd: " + quantidade + " nome: " + nome);
    

    // idReceita: string, idPrescricao: number, nAviar: number, nome: string
    this.sprescricoesService.aviarPorID(idReceita, idPrescricao, quantidade, nome )
    .subscribe(
      prescricao => {
        //if (prescricao.idPrescricao == idPrescricao) {  // PARA EVITAR QUANDO DEVOLVE O ERRADO.
          this.prescricao = null;
          this.prescricao = prescricao;
          this.estadoPrescricao = "Aviado: " + idReceita + "/" + idPrescricao;
        //}
        this.messageService.add("C: verReceita (Aviar) Observou isto: " + JSON.stringify(prescricao));
      }
      ,
      (err: HttpErrorResponse) => {
        if (err.error instanceof Error) {
          // A client-side or network error occurred. Handle it accordingly.
          this.messageService.add("An error occurred:" + err.error.message);
          this.estadoPrescricao = "Erro";
          
        } else {
          // The backend returned an unsuccessful response code.
          // The response body may contain clues as to what went wrong,
          var mem: string = `Backend returned code ${err.status}, body was: ${err.error}`;
          this.messageService.add(mem);
          this.estadoPrescricao = "Erro";
          try{        this.messageService.add(JSON.stringify(err));}
          catch(algo){this.messageService.add("Excepcao apanhada.");};
        }
      }
    );
  }

}
