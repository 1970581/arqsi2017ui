import { Injectable } from '@angular/core';

import { MessageService } from './message.service';
import { HttpClient, HttpResponse, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Subject } from 'rxjs';
import { Observable } from 'rxjs/Observable';
import { Configuracao } from './configuracao';
import { Medicamento } from './objetos/medicamento';
import { Apresentacao } from './objetos/apresentacao';
import { Posologia } from './objetos/posologia';
import { Farmaco } from './objetos/farmaco';

@Injectable()
export class SmedicamentoService {

  configuracao = new Configuracao();

  constructor(public messageService: MessageService , private http: HttpClient ) { }

  //Obtem uma lista de medicamentos indicando o nome. Se nome for vazio, devolve todos.
  public getListaMedicamentos(token:string, nomeMedicamento: string): Observable<Medicamento[]>{

    var caminho: string = this.configuracao.receitaurl + this.configuracao.getMedicamentosPorNome + nomeMedicamento;

    this.messageService.add("S medicamento: getListaMedicamentos " + caminho);

    let headers: HttpHeaders = new HttpHeaders({ 'Authorization': token, "Content-Type": "application/json" });

    return this.http.get<Medicamento[]>(caminho, { headers });
  }


  //Obtem lista de apresentacoes de um medicamento de ID
  public getListaApresentacaoDeMedicamentoDeID(token: string, idMedicamento: number): Observable<Apresentacao[]> {

    //gestaomedicamento/Medicamentos/7/apresentacoes
    var caminho: string = this.configuracao.receitaurl + this.configuracao.getMedicamentos + idMedicamento + "/apresentacoes";

    this.messageService.add("S medicamento: getListaApresentacaoDeMedicamentoDeID " + caminho);

    let headers: HttpHeaders = new HttpHeaders({ 'Authorization': token, "Content-Type": "application/json" });

    return this.http.get<Apresentacao[]>(caminho, { headers });
  }    

  //Obtem lista de posologias de um medicamento de id.
  public getListaPosologiasDeMedicamentoDeID(token: string, idMedicamento: number): Observable<Posologia[]> {
    //gestaomedicamento/Medicamentos/7/posologias
    var caminho: string = this.configuracao.receitaurl + this.configuracao.getMedicamentos + idMedicamento + "/posologias";

    this.messageService.add("S medicamento: getListaApresentacaoDeMedicamentoDeID " + caminho);

    let headers: HttpHeaders = new HttpHeaders({ 'Authorization': token, "Content-Type": "application/json" });

    return this.http.get<Posologia[]>(caminho, { headers });
  }   


  //Obter lista de farmacos de nome: se for vazio, devolve todos.
  public getListaFarmacosPorNome(token: string, nomeFarmaco: string): Observable<Farmaco[]> {

    var caminho: string = this.configuracao.receitaurl + this.configuracao.getFarmacosPorNome + nomeFarmaco;

    this.messageService.add("S medicamento: getListaFarmacosPorNome " + caminho);

    let headers: HttpHeaders = new HttpHeaders({ 'Authorization': token, "Content-Type": "application/json" });

    return this.http.get<Farmaco[]>(caminho, { headers });
  }


  //Obtem lista de apresentacoes de um farmaco de ID
  public getListaApresentacaoDeFarmacoDeID(token: string, idFarmaco: number): Observable<Apresentacao[]> {

    //gestaomedicamento/farmacos/2/apresentacoes
    var caminho: string = this.configuracao.receitaurl + this.configuracao.getFarmacos + idFarmaco + "/apresentacoes";

    this.messageService.add("S medicamento: getListaApresentacaoDeFarmacosDeID " + caminho);

    let headers: HttpHeaders = new HttpHeaders({ 'Authorization': token, "Content-Type": "application/json" });

    return this.http.get<Apresentacao[]>(caminho, { headers });
  }

  //Obtem lista de apresentacoes de um farmaco de ID
  public getListaPosologiaDeFarmacoDeID(token: string, idFarmaco: number): Observable<Posologia[]> {

    //gestaomedicamento/farmacos/2/apresentacoes
    var caminho: string = this.configuracao.receitaurl + this.configuracao.getFarmacos + idFarmaco + "/posologias";

    this.messageService.add("S medicamento: getListaPosologiasDeFarmacosDeID " + caminho);

    let headers: HttpHeaders = new HttpHeaders({ 'Authorization': token, "Content-Type": "application/json" });

    return this.http.get<Posologia[]>(caminho, { headers });
  }

}
