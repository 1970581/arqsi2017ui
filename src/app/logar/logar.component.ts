import { Component, OnInit } from '@angular/core';
import { LoginPessoa } from '../objetos/loginPessoa';
import { MessageService } from '../message.service';
import { AtoresService } from '../atores.service';

@Component({
  selector: 'app-logar',
  templateUrl: './logar.component.html',
  styleUrls: ['./logar.component.css']
})

//public let token;
//var loginPessoa = new LoginPessoa();

export class LogarComponent implements OnInit {

  objLoginPessoa; 
  titulo = "Login"

  constructor(
    private messageService: MessageService,
    public atoresService: AtoresService
    ) { }

  ngOnInit() {
    this.objLoginPessoa = new LoginPessoa;


  }

  // Invoca o servico para fazer o login.
  fazerLogin( nome , pass): void {    
    this.messageService.add('butao carregado.');
    this.atoresService.fazerLogin(nome, pass);
  }


}
