import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaranonimoComponent } from './listaranonimo.component';

describe('ListaranonimoComponent', () => {
  let component: ListaranonimoComponent;
  let fixture: ComponentFixture<ListaranonimoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaranonimoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaranonimoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
