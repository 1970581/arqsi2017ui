import { Component, OnInit } from '@angular/core';
import { MessageService } from '../message.service';
import { AtoresService } from '../atores.service';
import { SmedicamentoService } from '../smedicamento.service';
import { HttpClient, HttpResponse, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

import { Medicamento } from '../objetos/medicamento';
import { Apresentacao } from '../objetos/apresentacao';
import { Posologia } from '../objetos/posologia';

@Component({
  selector: 'app-listaranonimo',
  templateUrl: './listaranonimo.component.html',
  styleUrls: ['./listaranonimo.component.css']
})
export class ListaranonimoComponent implements OnInit {

  nomeMedicamento: string = "";

  medicamentos: Medicamento[];
  estadoMedicamentos:string = "";

  apresentacoes: Apresentacao[];
  estadoApresentacoes:string = "";

  posologias: Posologia[];
  estadoPosologias:string = "";

  constructor(
    private messageService: MessageService,
    public atoresService: AtoresService,
    private smedicamentoService: SmedicamentoService
  ) { }

  ngOnInit() {
    this.atoresService.readyTokenAnonimo();
  }

  // Obtem a lista de medicamentos com base no nome. Se nome vazio devolve todos.
  getListaMedicamentos(){

    this.estadoMedicamentos = "Procurando medicamentos... ";
    this.apresentacoes = null; this.estadoApresentacoes = "";
    this.posologias = null; this.estadoPosologias = "";

    this.smedicamentoService.getListaMedicamentos(this.atoresService.anonToken,this.nomeMedicamento)
    .subscribe(
      medicamentos => {
        
        this.medicamentos = medicamentos;
        this.estadoMedicamentos = "Encontrados " + medicamentos.length + "." ;
        
        this.messageService.add("C: listarMedicamentos (Medicamentos) Observou isto: " + JSON.stringify(medicamentos));
      }
      ,
      (err: HttpErrorResponse) => {
        if (err.error instanceof Error) {
          // A client-side or network error occurred. Handle it accordingly.
          this.messageService.add("C: listarMedicamentos An error occurred:" + err.error.message);
          this.estadoMedicamentos = "Erro";
          
        } else {
          // The backend returned an unsuccessful response code.
          // The response body may contain clues as to what went wrong,
          var mem: string = `C: listarMedicamentos Backend returned code ${err.status}, body was: ${err.error}`;
          this.messageService.add(mem);
          this.estadoMedicamentos = "Erro";
          try{        this.messageService.add(JSON.stringify(err));}
          catch(algo){this.messageService.add("Excepcao apanhada.");};
        }
      }
    );

  }


  //Obtem uma listas de apresentacoes do medicamento de ID
  public getListaApresentacaoDeMedicamentoDeID(idMedicamento: number){

    this.estadoApresentacoes = "Procurando... "  ;

    this.smedicamentoService.getListaApresentacaoDeMedicamentoDeID(this.atoresService.anonToken,idMedicamento)
    .subscribe(
      apresentacoes => {
        
        this.apresentacoes = apresentacoes;
        this.estadoApresentacoes = "Encontradas " + apresentacoes.length + "." ;
        
        this.messageService.add("C: listarMedicamentos (Apresentacoes) Observou isto: " + JSON.stringify(apresentacoes));
      }
      ,
      (err: HttpErrorResponse) => {
        if (err.error instanceof Error) {
          // A client-side or network error occurred. Handle it accordingly.
          this.messageService.add("C: listarMedicamentos An error occurred:" + err.error.message);
          this.estadoApresentacoes = "Erro";
          
        } else {
          // The backend returned an unsuccessful response code.
          // The response body may contain clues as to what went wrong,
          var mem: string = `C: listarMedicamentos Backend returned code ${err.status}, body was: ${err.error}`;
          this.messageService.add(mem);
          this.estadoApresentacoes = "Erro";
          try{        this.messageService.add(JSON.stringify(err));}
          catch(algo){this.messageService.add("Excepcao apanhada.");};
        }
      }
    );

  }
    //Obtem uma listas de posologias do medicamento de ID
    public getListaPosologiasDeMedicamentoDeID(idMedicamento: number){

      this.estadoPosologias = "Procurando... ";

      this.smedicamentoService.getListaPosologiasDeMedicamentoDeID(this.atoresService.anonToken, idMedicamento)
        .subscribe(
        posologias => {

          this.posologias = posologias;
          this.estadoPosologias = "Encontradas " + posologias.length + ".";

          this.messageService.add("C: listarMedicamentos (Posologias) Observou isto: " + JSON.stringify(posologias));
        }
        ,
        (err: HttpErrorResponse) => {
          if (err.error instanceof Error) {
            // A client-side or network error occurred. Handle it accordingly.
            this.messageService.add("C: listarMedicamentos An error occurred:" + err.error.message);
            this.estadoPosologias = "Erro";

          } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            var mem: string = `C: listarMedicamentos Backend returned code ${err.status}, body was: ${err.error}`;
            this.messageService.add(mem);
            this.estadoPosologias = "Erro";
            try { this.messageService.add(JSON.stringify(err)); }
            catch (algo) { this.messageService.add("Excepcao apanhada."); };
          }
        }
        );


    }


    public posologiaToString(pos:Posologia){
      //return JSON.stringify(pos.toString());
      return "Tomar " + pos.dosagem + " de " + pos.intervalo + " em " + pos.intervalo + " horas durante " + pos.duracao + " dias.";
    }
}
