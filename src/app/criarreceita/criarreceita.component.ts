import { Component, OnInit } from '@angular/core';
import { MessageService } from '../message.service';
import { AtoresService } from '../atores.service';
import { SreceitasService } from '../sreceitas.service';
import { Receita} from '../objetos/receita';
import { NovaReceita} from '../objetos/novaReceita';
import { NovaPrescricao} from '../objetos/novaPrescricao';

import { HttpClient, HttpResponse, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { SmedicamentoService } from '../smedicamento.service';
import { Medicamento } from '../objetos/medicamento';
import { Apresentacao } from '../objetos/apresentacao';
import { Posologia } from '../objetos/posologia';
import { Farmaco } from '../objetos/farmaco';

@Component({
  selector: 'app-criarreceita',
  templateUrl: './criarreceita.component.html',
  styleUrls: ['./criarreceita.component.css']
})
export class CriarreceitaComponent implements OnInit {

  //public receitaCriar: NovaReceita = new NovaReceita();

  public nomeUtente : string = "utentez"; 
  public prescricoes: NovaPrescricao[] = new Array();

  
  

  constructor(
    private messageService: MessageService, 
    //private atoresService: AtoresService, 
    public atoresService: AtoresService, 
    private sreceitasService: SreceitasService,
    private smedicamentoService: SmedicamentoService
  ) { }

  ngOnInit() {    
  }

  public criarNovaReceita(){
    var erro : string;    
    if(this.prescricoes.length == 0) erro = "Sem prescricoes.";
    if(erro){this.messageService.add("C: criarReceita: FAIL " + erro); return;}

    var bodyObj = { utente: this.nomeUtente, prescricao: this.prescricoes };
    var body : string = JSON.stringify(bodyObj);
    this.messageService.add("Receita a criar:" + body);
    this.sreceitasService.criarReceitaMedico(body);
    //this.sreceitasService.criarReceita(JSON.bodyObj);
    
  }

  public editarReceita(){
    var erro : string;
    if(this.sreceitasService.sugestaoReceita.length == 0) erro = "Id receita editar vazia.";
    if(this.prescricoes.length == 0) erro = "Sem prescricoes.";
    if(erro){this.messageService.add("C: editarReceita: FAIL " + erro); return;}

    var bodyObj = { utente: this.nomeUtente, prescricao: this.prescricoes };
    var body : string = JSON.stringify(bodyObj);
    this.messageService.add("Receita a editar:" + body);
    this.sreceitasService.editarReceitaMedico(body, this.sreceitasService.sugestaoReceita);

  }



  public novaPrescricao(){
    this.prescricoes.push(new NovaPrescricao());
  }

  public apagarUltimaPrescricao(){
    this.prescricoes.pop();
  }

  public imprimirDebug(){  this.messageService.add("C criarreceita: " + this.nomeUtente + " " + JSON.stringify(this.prescricoes));}





  //=================================================================
  // Parte para o medico ter info sobre os farmacos.

  farmacos: Farmaco[];
  estadoFarmacos:string = "";

  apresentacoes: Apresentacao[];
  estadoApresentacoes:string = "";

  posologias: Posologia[];
  estadoPosologias:string = "";

  nomeFarmaco: string = "";

  //Obtem uma lista de farmacos baseado no nome dele ou todos se o nome for vazio.
  public getListaFarmacosPorNome() {

    this.estadoFarmacos = "Procurando farmacos... ";
    this.apresentacoes = null; this.estadoApresentacoes = "";
    this.posologias = null; this.estadoPosologias = "";

    this.smedicamentoService.getListaFarmacosPorNome(this.atoresService.myToken, this.nomeFarmaco)
      .subscribe(
      farmacos => {

        this.farmacos = farmacos;
        this.estadoFarmacos = "Encontrados " + farmacos.length + ".";

        this.messageService.add("C: criarReceita (Farmacos) Observou isto: " + JSON.stringify(farmacos));
      }
      ,
      (err: HttpErrorResponse) => {
        if (err.error instanceof Error) {
          // A client-side or network error occurred. Handle it accordingly.
          this.messageService.add("C: criarReceita An error occurred:" + err.error.message);
          this.estadoFarmacos = "Erro";

        } else {
          // The backend returned an unsuccessful response code.
          // The response body may contain clues as to what went wrong,
          var mem: string = `C: criarReceita Backend returned code ${err.status}, body was: ${err.error}`;
          this.messageService.add(mem);
          this.estadoFarmacos = "Erro";
          try { this.messageService.add(JSON.stringify(err)); }
          catch (algo) { this.messageService.add("Excepcao apanhada."); };
        }
      }
      );

  }

  //Obtem uma lista de apresentacoes do farmaco de ID...
  public getListaApresentacoesDeFarmacoDeID( idFarmaco:number){
    this.estadoApresentacoes = "Procurando... "  ;
    
        this.smedicamentoService.getListaApresentacaoDeFarmacoDeID(this.atoresService.myToken,idFarmaco)
        .subscribe(
          apresentacoes => {
            
            this.apresentacoes = apresentacoes;
            this.estadoApresentacoes = "Encontradas " + apresentacoes.length + "." ;
            
            this.messageService.add("C: criarReceita (Apresentacoes) Observou isto: " + JSON.stringify(apresentacoes));
          }
          ,
          (err: HttpErrorResponse) => {
            if (err.error instanceof Error) {
              // A client-side or network error occurred. Handle it accordingly.
              this.messageService.add("C: criarReceita An error occurred:" + err.error.message);
              this.estadoApresentacoes = "Erro";
              
            } else {
              // The backend returned an unsuccessful response code.
              // The response body may contain clues as to what went wrong,
              var mem: string = `C: criarReceita Backend returned code ${err.status}, body was: ${err.error}`;
              this.messageService.add(mem);
              this.estadoApresentacoes = "Erro";
              try{        this.messageService.add(JSON.stringify(err));}
              catch(algo){this.messageService.add("Excepcao apanhada.");};
            }
          }
        );
  }

  //obtem uma lista de prescricoes deste farmaco por id.
  public getListaPosologiasDeFarmacoDeID(idFarmaco: number) {


    this.estadoPosologias = "Procurando... ";

    this.smedicamentoService.getListaPosologiaDeFarmacoDeID(this.atoresService.myToken, idFarmaco)
      .subscribe(
      posologias => {

        this.posologias = posologias;
        this.estadoPosologias = "Encontradas " + posologias.length + ".";

        this.messageService.add("C: criarReceita (Posologias) Observou isto: " + JSON.stringify(posologias));
      }
      ,
      (err: HttpErrorResponse) => {
        if (err.error instanceof Error) {
          // A client-side or network error occurred. Handle it accordingly.
          this.messageService.add("C: criarReceita An error occurred:" + err.error.message);
          this.estadoPosologias = "Erro";

        } else {
          // The backend returned an unsuccessful response code.
          // The response body may contain clues as to what went wrong,
          var mem: string = `C: criarReceita Backend returned code ${err.status}, body was: ${err.error}`;
          this.messageService.add(mem);
          this.estadoPosologias = "Erro";
          try { this.messageService.add(JSON.stringify(err)); }
          catch (algo) { this.messageService.add("Excepcao apanhada."); };
        }
      }
      );

  }

  //Cria uma nova prescricao e adiciona o o IdApresentacao automaticamente.
  public presceverApresentacao(idApresentacao:number){
    this.novaPrescricao();
    let n:number = this.prescricoes.length -1;
    this.prescricoes[n].idApresentacao = idApresentacao;
  }

  public posologiaToString(pos:Posologia){
    //return JSON.stringify(pos.toString());
    return "Tomar " + pos.dosagem + " de " + pos.intervalo + " em " + pos.intervalo + " horas durante " + pos.duracao + " dias.";
  }

  //Copia a posologia para a ultima prescricao.
  public copiarPosologia(pos: Posologia) {
    try {
      if (this.prescricoes) {
        if (this.prescricoes.length > 0) {
          let n = this.prescricoes.length;
          this.prescricoes[n-1].posologia = this.posologiaToString(pos);
        }
      }
    }
    catch (erro) { this.messageService.add("Erro a copiar prescricao."); }
  }

}
