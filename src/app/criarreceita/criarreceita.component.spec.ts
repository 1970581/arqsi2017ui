import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CriarreceitaComponent } from './criarreceita.component';

describe('CriarreceitaComponent', () => {
  let component: CriarreceitaComponent;
  let fixture: ComponentFixture<CriarreceitaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CriarreceitaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CriarreceitaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
