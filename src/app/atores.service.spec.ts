import { TestBed, inject } from '@angular/core/testing';

import { AtoresService } from './atores.service';

describe('AtoresService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AtoresService]
    });
  });

  it('should be created', inject([AtoresService], (service: AtoresService) => {
    expect(service).toBeTruthy();
  }));
});
