import { Component, OnInit } from '@angular/core';
import { MessageService } from '../message.service';
import { AtoresService } from '../atores.service';
import { SreceitasService } from '../sreceitas.service';
import { Receita} from '../objetos/receita';

@Component({
  selector: 'app-listarreceitas',
  templateUrl: './listarreceitas.component.html',
  styleUrls: ['./listarreceitas.component.css']
})
export class ListarreceitasComponent implements OnInit {

  //export const HEROES: Hero[] =
  receitas: Receita[];

  constructor(private messageService: MessageService, 
    public atoresService: AtoresService, 
    private sreceitasService: SreceitasService) { }

  ngOnInit() {
    //this.listaReceitas = new Array();
  }

  //Obtem as receitas.
  public obterReceitas(){

    this.sreceitasService.getMyReceitas() .subscribe(receitas => { this.receitas = receitas; });
    this.messageService.add(JSON.stringify(this.receitas));

  }

  public debugShow(){
    this.messageService.add("test");
    this.messageService.add(JSON.stringify(this.receitas));
  }

  public imprimir(texto:string): string{
    return texto;
  }

  // Indica ao servico qual a sugestao de receita.
  public sugestao(idReceita:string){
    this.sreceitasService.sugestaoReceita = idReceita;
  }

}
