import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListarreceitasComponent } from './listarreceitas.component';

describe('ListarreceitasComponent', () => {
  let component: ListarreceitasComponent;
  let fixture: ComponentFixture<ListarreceitasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListarreceitasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarreceitasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
