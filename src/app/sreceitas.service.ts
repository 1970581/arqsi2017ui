import { Injectable } from '@angular/core';

import { MessageService } from './message.service';
import { AtoresService } from './atores.service';
import { Receita } from './objetos/receita';
import { HttpClient, HttpResponse, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Subject } from 'rxjs';
import { Observable } from 'rxjs/Observable';
import { Configuracao } from './configuracao';




@Injectable()
export class SreceitasService {

  //Usado globalmente para guardar o ID da receita que o utilizador mostrou interesse.
  sugestaoReceita: string = "";

  //Relevantes a criar/editar receita, mensagem de feedback.
  criarReceitaMessage: string = "";
  editarReceitaMessage: string = "";
  buscarReceitaMessage: string = "";

  configuracao = new Configuracao();

  constructor(private messageService: MessageService, private atoresService: AtoresService, private http: HttpClient ) { }

  //Obtem a header de autentificacao.
  getHeaders(){
    let headers = new HttpHeaders({'Authorization':
    this.atoresService.myToken });
    let httpOptions = {
    headers: headers
    };
    return httpOptions;
  }
    

  // Obtem as receitas do token que esta logado.
  public getMyReceitas()  : Observable<Receita[]> {    

    var caminho: string = this.configuracao.receitaurl + this.configuracao.getReceitas;    

    return this.http.get<Receita[]> ( caminho, this.getHeaders());

  }

  //Interface de acesso publico para criar Receita.
  public criarReceitaMedico(body: string){
    this.criarReceita(body);
  }

  //Metodo para criar uma nova receita com base num body string.
  private criarReceita(body : string) {

    this.criarReceitaMessage = "Criando.....";
    this.editarReceitaMessage = "";


    var caminho: string = this.configuracao.receitaurl + this.configuracao.criarReceita;
    
    let headers :HttpHeaders = new HttpHeaders({'Authorization': this.atoresService.myToken, "Content-Type":"application/json" });

    this.http.post<Receita>(caminho, body, { headers , observe: 'response'} )
    
      .subscribe(responce => {

        if (responce.status == 201) {  // Portanto code 201  
//        if (responce.body) {    // Se tivermos body.
  
            this.messageService.add("Receita criada com sucesso!!!!!!");
            this.criarReceitaMessage = "Criada";
                        
        }
        if (responce.body) {
          this.sugestaoReceita = responce.body.idReceita;
          this.criarReceitaMessage = "Criada " + responce.body.idReceita;
          this.messageService.add("S: receita recebida: "+ JSON.stringify(responce.body));
        }
      },

    (err: HttpErrorResponse) => {
      if (err.error instanceof Error) {
        // A client-side or network error occurred. Handle it accordingly.
        this.messageService.add("An error occurred:" + err.error.message);
        this.criarReceitaMessage = "Erro.";
        
      } else {
        // The backend returned an unsuccessful response code.
        // The response body may contain clues as to what went wrong,
        var mem: string = `Backend returned code ${err.status}, body was: ${err.error}`;
        this.messageService.add(mem);
        this.criarReceitaMessage = "Erro";
        try{        this.messageService.add(JSON.stringify(err));}
        catch(algo){this.messageService.add("Excepcao apanhada.");};
      }
    }
  );

  }

  //Interface de acesso publico para editar Receita.
  public editarReceitaMedico(body: string, idReceita: string){
    this.editarReceita(body, idReceita);
  }

  //Metodo para editar uma nova receita existente com base num body string e o ID da receita.
  public editarReceita(body: string, idReceita: string){
    this.criarReceitaMessage = "";
    this.editarReceitaMessage = "Editando.....";

    var caminho: string = this.configuracao.receitaurl + this.configuracao.criarReceita + "/" + idReceita;
    
    let headers :HttpHeaders = new HttpHeaders({'Authorization': this.atoresService.myToken, "Content-Type":"application/json" });

    this.http.put<Receita>(caminho, body, { headers , observe: 'response'} )

    .subscribe(responce => {
      
              if (responce.status == 201) {  // Portanto code 201  
      //        if (responce.body) {    // Se tivermos body.
        
                  this.messageService.add("Receita editada com sucesso!!!!!!");                  
                              
              }
              if (responce.body) {
                this.sugestaoReceita = responce.body.idReceita;
                this.editarReceitaMessage = "Editada " + responce.body.idReceita;
                this.messageService.add("S: receita recebida: "+ JSON.stringify(responce.body));
              }
            },
      
          (err: HttpErrorResponse) => {
            if (err.error instanceof Error) {
              // A client-side or network error occurred. Handle it accordingly.
              this.messageService.add("An error occurred:" + err.error.message);
              this.editarReceitaMessage = "Falhou edicao";
              
            } else {
              // The backend returned an unsuccessful response code.
              // The response body may contain clues as to what went wrong,
              var mem: string = `Backend returned code ${err.status}, body was: ${err.error}`;
              this.messageService.add(mem);
              this.editarReceitaMessage = "Falhou edicao";
              try{        this.messageService.add(JSON.stringify(err));}
              catch(algo){this.messageService.add("Excepcao apanhada.");};
            }
          }
        );




  }


  //Busca uma unica receita por ID e devolve um observador.
  public buscarReceitaPorID(idReceita: string) : Observable<Receita> {    
        
    var caminho: string = this.configuracao.receitaurl + this.configuracao.getReceitas +"/" + idReceita;    
    
    return this.http.get<Receita> ( caminho, this.getHeaders());
  }


}
