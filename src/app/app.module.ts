import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'; // <-- NgModel lives here

import { AppComponent } from './app.component';
import { LogarComponent } from './logar/logar.component';
import { MessageService } from './message.service';
import { MessagesComponent } from './messages/messages.component';
import { AppRoutingModule } from './/app-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AtoresService } from './atores.service';
// Import HttpClientModule from @angular/common/http
import {HttpClientModule} from '@angular/common/http';
import { RegistarComponent } from './registar/registar.component';
import { ListarreceitasComponent } from './listarreceitas/listarreceitas.component';
import { SreceitasService } from './sreceitas.service';
import { CriarreceitaComponent } from './criarreceita/criarreceita.component';
import { VerreceitaComponent } from './verreceita/verreceita.component';
import { SprescricoesService } from './sprescricoes.service';
import { ListaranonimoComponent } from './listaranonimo/listaranonimo.component';
import { SmedicamentoService } from './smedicamento.service';
import { SutenteService } from './sutente.service';
import { UtenteporaviarComponent } from './utenteporaviar/utenteporaviar.component';


@NgModule({
  declarations: [
    AppComponent,
    LogarComponent,
    MessagesComponent,
    DashboardComponent,
    RegistarComponent,
    ListarreceitasComponent,
    CriarreceitaComponent,
    VerreceitaComponent,
    ListaranonimoComponent,
    UtenteporaviarComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [MessageService, AtoresService, SreceitasService, SprescricoesService, SmedicamentoService, SutenteService],
  bootstrap: [AppComponent]
})
export class AppModule { }
