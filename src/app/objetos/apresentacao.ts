export class Apresentacao {    
    id:         number;
    idFarmaco:  number;
    nomeFarmaco: string;
    forma:      string;
    concentracao: string;
    qtd:        string;                
}

/*
{
        "id": 12,
        "idFarmaco": 6,
        "nomeFarmaco": "f01",
        "forma": "xarope",
        "concentracao": "15mg",
        "qtd": "75"
    }
*/