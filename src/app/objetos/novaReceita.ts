import { NovaPrescricao} from './novaPrescricao';
export class NovaReceita {
    public utente:         string;    
    public prescricao: NovaPrescricao[];
    constructor(){};
}
/*
export class NovaReceita {
     
    utente:         string;    
    prescricao: [{
        idApresentacao: number,
        posologia:      string,                
        quantidade:     number,
        nomeMedicamento: string               
    }]    
  }
*/


  /*
  	"utente": "utente",
	"prescricao": [
		{
        	"idApresentacao":	1,
        	"posologia":    	"Um comprimido ao deitar.",
        	"quantidade":		5
		},
		{
        	"idApresentacao":	5,
        	"posologia":    	"Uma colher de sopa as refeicoes.",
        	"quantidade":		1,
        	"nomeMedicamento": "Cegripe"
		},
		{
        	"idApresentacao":	5,
        	"posologia":    	"Uma colher de antes de ir para a escola.",
        	"quantidade":		15
		}
		]
  */