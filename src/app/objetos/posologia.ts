export class Posologia {    
    id:         number;
    idApresentacao:  number;
    duracao: number;
    intervalo:      number;
    dosagem: number;    
    
    public toString():string{
        let texto:string = "Tomar " + this.dosagem + " de " +this.intervalo + " horas em" + this.intervalo + " horas durante " + this.duracao + " dias.";
        return texto;
    }
}

/*
{
        "id": 6,
        "idApresentacao": 11,
        "duracao": 8,
        "intervalo": 12,
        "dosagem": 5
    },
*/