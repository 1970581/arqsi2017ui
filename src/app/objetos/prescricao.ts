export class Prescricao {

        idPrescricao:    number;
        idFarmaco:       number;
        nomeFarmaco:         String;
        nomeMedicamento:     String;
        posologia:      string;
        quantidade:     number;
        aviados:        number;
        idApresentacao: number;
        apresentacao:   
        {
            id:         number;
            concentracao:       String;
            forma:              String;
            qtd:                String;
        };
        aviamentos: [{            
            nomeFarmaceutico: string; 
            nomeMedicamento: string; 
            quantidade:     number;
            data: Date;
        }];
  }

  /*
  {
    "idPrescricao": 1,
    "idFarmaco": 3,
    "nomeFarmaco": "Acetilsalicilato de lisina",
    "posologia": "Uma colher antes de dormir.",
    "quantidade": 1,
    "aviados": 1,
    "idApresentacao": 5,
    "apresentacao": {
        "id": 5,
        "concentracao": "10mg",
        "forma": "comprimidos",
        "qtd": "24"
    },
    "nomeMedicamento": "Cegripe",
    "aviamentos": [
        {
            "nomeFarmaceutico": "farmaceutico",
            "quantidade": 1,
            "data": "2017-11-10T15:59:47.000Z",
            "nomeMedicamento": "Cegripe"
        }
    ]
}
  */
  