export class Prescricaoporaviar {    
    type:   string;
    idReceita: string;
    data:   Date;
    idPrescricao: number;
    idFarmaco:  number;
    nomeFarmaco: string;
    posologia: string;
    quantidade: number;
    aviados: number;
    idApresentacao: number;
    apresentacao:{
        id: number;
        concentracao: string;
        forma: string;
        qtd: string;
    }
}

/*
        "type": "prescricao por aviar",
        "idReceita": "5a04d9a612e29f2534883fd7",
        "data": "2017-11-09T22:41:42.000Z",
        "idPrescricao": 2,
        "idFarmaco": 3,
        "nomeFarmaco": "Acetilsalicilato de lisina",
        "posologia": "Uma colher de antes de ir para a escola.",
        "quantidade": 15,
        "aviados": 0,
        "idApresentacao": 5,
        "apresentacao": {
            "id": 5,
            "concentracao": "10mg",
            "forma": "comprimidos",
            "qtd": "24"
        }
*/