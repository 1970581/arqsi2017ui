export class Receita {
    idReceita:          string;
    idMedico:           string;
    nomeMedico:         String;
    idUtente:           string;
    nomeUtente:         String;
    emailUtente:        String;
    data:               Date;
    hasAviamentos:      boolean;
    prescricoes: [{
        idPrescricao:    number;
        idFarmaco:       number;
        nomeFarmaco:         String;
        nomeMedicamento:     String;
        posologia:      string;
        quantidade:     number;
        aviados:        number;
        idApresentacao: number;
        apresentacao:   
        {
            id:         number;
            concentracao:       String;
            forma:              String;
            qtd:                String;
        },
        aviamentos: [{
            idFarmaceutico:           string;
            nomeFarmaceutico: string; 
            nomeMedicamento: string; 
            quantidade:     number;
            data: Date;
        }]
    }]
    
  }