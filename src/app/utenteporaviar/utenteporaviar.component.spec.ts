import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UtenteporaviarComponent } from './utenteporaviar.component';

describe('UtenteporaviarComponent', () => {
  let component: UtenteporaviarComponent;
  let fixture: ComponentFixture<UtenteporaviarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UtenteporaviarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UtenteporaviarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
