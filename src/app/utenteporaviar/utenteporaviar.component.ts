import { Component, OnInit } from '@angular/core';

import { MessageService } from '../message.service';
import { AtoresService } from '../atores.service';
import { SreceitasService } from '../sreceitas.service';
import { Receita} from '../objetos/receita';



import { HttpClient, HttpResponse, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { SutenteService } from '../sutente.service';

import { Prescricaoporaviar } from '../objetos/prescricaoporaviar';

@Component({
  selector: 'app-utenteporaviar',
  templateUrl: './utenteporaviar.component.html',
  styleUrls: ['./utenteporaviar.component.css']
})
export class UtenteporaviarComponent implements OnInit {

  nomeUtente:string = "";
  data:string = "";


  prescricoes: Prescricaoporaviar[];
  estadoPrescricoes:string = "";

  constructor(
    private messageService: MessageService, 
    public atoresService: AtoresService,
    private sutenteService: SutenteService,
    private sreceitasService: SreceitasService
  ) { }

  ngOnInit() {
  }

  //Obtem uma lista de prescricoes do utente indicado criadas depois de:
  public getListaPrescricoesDeUtenteDepoisDe() {

    //let token:string = this.atoresService.myToken;


    //this.getListaPrescricoesDeUtenteDepoisDe
    this.prescricoes = null;
    this.estadoPrescricoes = "Procurando... ";

    this.sutenteService.getListaPrescricoesDeUtenteDepoisDe(this.atoresService.myToken, this.nomeUtente, this.data)
      .subscribe(
      prescricoes => {

        this.prescricoes = prescricoes;
        this.estadoPrescricoes = "Encontradas " + prescricoes.length + ".";

        this.messageService.add("C: utentePorAviar (Posologias) Observou isto: " + JSON.stringify(prescricoes));
      }
      ,
      (err: HttpErrorResponse) => {
        if (err.error instanceof Error) {
          // A client-side or network error occurred. Handle it accordingly.
          this.messageService.add("C: utentePorAviar An error occurred:" + err.error.message);
          this.estadoPrescricoes = "Erro";

        } else {
          // The backend returned an unsuccessful response code.
          // The response body may contain clues as to what went wrong,
          var mem: string = `C: utentePorAviar Backend returned code ${err.status}, body was: ${err.error}`;
          this.messageService.add(mem);
          this.estadoPrescricoes = "Erro";
          try { this.messageService.add(JSON.stringify(err)); }
          catch (algo) { this.messageService.add("Excepcao apanhada."); };
        }
      }
      );

  }

  // Indica ao servico qual a sugestao de receita.
  public sugestao(idReceita: string) {
    this.sreceitasService.sugestaoReceita = idReceita;
  }




}
