import { Injectable } from '@angular/core';

import { MessageService } from './message.service';
import { AtoresService } from './atores.service';
import { Receita } from './objetos/receita';
import { HttpClient, HttpResponse, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Subject } from 'rxjs';
import { Observable } from 'rxjs/Observable';
import { Configuracao } from './configuracao';

import { Prescricao} from './objetos/prescricao';

@Injectable()
export class SprescricoesService {

  configuracao = new Configuracao();

  constructor(private messageService: MessageService, private atoresService: AtoresService, private http: HttpClient) { };

  //Obtem a header de autentificacao.
  getHeaders(){
    let headers = new HttpHeaders({'Authorization':
    this.atoresService.myToken });
    let httpOptions = {
    headers: headers
    };
    return httpOptions;
  }


    //Busca uma unica receita por ID e devolve um observador.
  public buscarPrescricaoPorID(idReceita: string, idPrescricao: number) : Observable<Prescricao> {    
      
    var caminho: string = this.configuracao.receitaurl + this.configuracao.getReceitas +"/" + idReceita + "/prescricao/" + idPrescricao ;    
  
    return this.http.get<Prescricao> ( caminho, this.getHeaders());
  }

  //Aviar uma prescricao
  public aviarPorID(idReceita: string, idPrescricao: number, nAviar: number, nome: string): Observable<Prescricao> {

    var caminho: string = this.configuracao.receitaurl + this.configuracao.getReceitas +"/" + idReceita + "/prescricao/" + idPrescricao + "/aviar" ;    

    // { "quantidade": 1 , "nomeMedicamento": "Cegripe"}
    var bodyObj;
    
    if (nome) bodyObj = { quantidade: nAviar, nomeMedicamento: nome };
    else bodyObj = { quantidade: nAviar };
    var body : string = JSON.stringify(bodyObj);

    this.messageService.add("S sprescricoes: post Aviando "+ idReceita + "-"+ idPrescricao +" body" + body + " em " + caminho);
    
    let headers :HttpHeaders = new HttpHeaders({'Authorization': this.atoresService.myToken, "Content-Type":"application/json" });

    return this.http.put<Prescricao> ( caminho, body, { headers } );


  }


}
