import { TestBed, inject } from '@angular/core/testing';

import { SmedicamentoService } from './smedicamento.service';

describe('SmedicamentoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SmedicamentoService]
    });
  });

  it('should be created', inject([SmedicamentoService], (service: SmedicamentoService) => {
    expect(service).toBeTruthy();
  }));
});
