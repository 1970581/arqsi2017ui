import { Component, OnInit } from '@angular/core';
import { MessageService } from '../message.service';
import { AtoresService } from '../atores.service';
import { Pessoa } from '../objetos/pessoa';

@Component({
  selector: 'app-registar',
  templateUrl: './registar.component.html',
  styleUrls: ['./registar.component.css']
})
export class RegistarComponent implements OnInit {

  pessoa: Pessoa;

  constructor(
    private messageService: MessageService,
    public atoresService: AtoresService
    ) { }

  ngOnInit() {
    this.pessoa = new Pessoa();
  }

  //Registar a pessoa, alterada por HTML
  registar(){
    //this.messageService.add(JSON.stringify(this.pessoa));
    this.atoresService.registarPessoa(this.pessoa);
  }

  //codigo dos botoes
  setFarmaceutico(farma : boolean){this.pessoa.farmaceutico = farma;}
  setMedico(med : boolean){this.pessoa.medico = med;}

}
