import { Injectable } from '@angular/core';
import { MessageService } from './message.service';
import { Pessoa } from './objetos/pessoa';
import { HttpClient, HttpResponse, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Subject } from 'rxjs';
import { Observable } from 'rxjs/Observable';
import { Configuracao } from './configuracao';

//var configuracao2 = require('./configuracao');


// Este modulo e o servico que lida com Autentificacao, login e registos.

class TokenBody {
  success: boolean;
  message: string;
  token: string;
  medico: boolean;
  farmaceutico: boolean;  
  constructor(){}     
}


@Injectable()
export class AtoresService {

  //Se alguem esta logado:
  public algemLogado: boolean = false;

  // Variaveis da pessoa logada.
  public myNome: string;
  public myToken : string;
  public medico: boolean = false;
  public farmaceutico: boolean = false;

  // para registar uma nova pessoa.
  public myPessoa = new Pessoa();
  public aRegistarPessoa: boolean = false;
  public sucessoRegisto: string = "";

  public anonToken: string;

  //configuracao = require('./configuracao');
  configuracao = new Configuracao();

  private caminho = this.configuracao.receitaurl + this.configuracao.getToken;
  private caminhoRegistar = this.configuracao.receitaurl + "/pessoa/";

  constructor(public messageService: MessageService , private http: HttpClient ) { }

  // Interface publica para criar uma nova pessoa.
  public registarPessoa(novaPessoa: Pessoa){
    this.myPessoa = novaPessoa;
    this.registar();
  }

  //metodo privado para criar a pessoa.
  private registar(){
    this.aRegistarPessoa = true;
    this.sucessoRegisto = "...tentando........";
    this.messageService.add("Obj pessoa a criar: "  + JSON.stringify(this.myPessoa));
    var bodyR = { 
      "nome": this.myPessoa.nome,
      "password": this.myPessoa.password,
      "email": this.myPessoa.email,
      "medico": this.myPessoa.medico,
      "farmaceutico": this.myPessoa.farmaceutico
    };

    this.http.post<Pessoa>(this.caminhoRegistar, bodyR, {observe: 'response'})
    
      .subscribe(responce => {
        if (responce.body) {    // Se tivermos body.
          if (responce.status == 201) {  // Portanto code 201            
            
              
              this.messageService.add("Registo resposta: "  + JSON.stringify(responce.body));
              this.sucessoRegisto = "SUCESSO";
            }
        }        
      },

    (err: HttpErrorResponse) => {
      if (err.error instanceof Error) {
        // A client-side or network error occurred. Handle it accordingly.
        this.messageService.add("An error occurred:" + err.error.message);
        this.sucessoRegisto = "FALHOU";
      } else {
        // The backend returned an unsuccessful response code.
        // The response body may contain clues as to what went wrong,
        var mem: string = `Backend returned code ${err.status}, body was: ${err.error}`;
        this.messageService.add(mem);
        this.sucessoRegisto = "FALHOU";
      }
    }
  )
  }



  //Interface para fazer o login no servidor remoto, obtendo o token.
  public fazerLogin(nome: string, password: string){    
    this.login(nome, password );
  }

  //Faz o login mesmo.
  private login(nome: string, password: string) {
    
    var body = { "nome": nome, "password": password};

    this.http.post<TokenBody>(this.caminho, body, {observe: 'response'})
    
      .subscribe(responce => {
        if (responce.body) {    // Se tivermos body.
          if (responce.status == 200) {  // Portanto code 200            
            if (responce.body.token) {
              this.resetLoginData();
              this.myToken = responce.body.token;
              this.myNome = nome;
              this.algemLogado = true;
              this.messageService.add("Logando " + nome + " em " + this.caminho);
              this.messageService.add(nome + " "  + JSON.stringify(responce.body));
            }
            if (responce.body.medico) this.medico = responce.body.medico;
            if (responce.body.farmaceutico) this.farmaceutico = responce.body.farmaceutico;
          }
        }
      },

    (err: HttpErrorResponse) => {
      if (err.error instanceof Error) {
        // A client-side or network error occurred. Handle it accordingly.
        this.messageService.add("An error occurred:" + err.error.message);
      } else {
        // The backend returned an unsuccessful response code.
        // The response body may contain clues as to what went wrong,
        var mem: string = `Backend returned code ${err.status}, body was: ${err.error}`;
        this.messageService.add(mem);
      }
    }
  )

  }

  //Faz reset a data de login.
  resetLoginData(){
    this.messageService.add(this.myNome + " m: " + this.medico + " f: " + this.farmaceutico + " token: " + this.myToken);
    this.algemLogado = false;
    this.myNome = "";
    this.farmaceutico = false;
    this.medico = false;
    this.myToken = "";
  }

  //Faz o logout.
  public logout(){
    this.resetLoginData();
  }


  // Obtem o tokem do guest anonimo.
  public readyTokenAnonimo(){    
    let nome:string = this.configuracao.anonUser;
    let password: string = this.configuracao.anonPass;

    var body = { "nome": nome, "password": password};
    
        this.http.post<TokenBody>(this.caminho, body, {observe: 'response'})
        
          .subscribe(responce => {
            if (responce.body) {    // Se tivermos body.
              if (responce.status == 200) {  // Portanto code 200            
                if (responce.body.token) {
                  this.anonToken = responce.body.token;
                  this.messageService.add("S atores Funcao anonima ononline.");
                }                
              }
            }
          },
    
        (err: HttpErrorResponse) => {
          if (err.error instanceof Error) {
            // A client-side or network error occurred. Handle it accordingly.
            this.messageService.add("S atores An error occurred:" + err.error.message);
          } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            var mem: string = `S atores Backend returned code ${err.status}, body was: ${err.error}`;
            this.messageService.add(mem);
          }
        }
      )
    

  }


/*
  login(email: string, password: string): Observable<boolean> {
    return new Observable<boolean>(observer => {
      this.http.post<Token>(this.authUrl, { email: email, password: password })
        .subscribe(data => {
          if (data.token) {
            const tokenDecoded = jwt_decode(data.token);
            this.userInfo = {
              token: data.token,
              tokenExp: tokenDecoded.exp,
              medico: tokenDecoded.medico,
              farmaceutico: tokenDecoded.farmaceutico,
              utente: tokenDecoded.utente
            }
            localStorage.userInfo = this.userInfo;
            this.authentication.next(this.userInfo);
            observer.next(true);
          } else {
            this.authentication.next(this.userInfo);
            observer.next(false);
          }
        },
        (err: HttpErrorResponse) => {
          if (err.error instanceof Error) {
            console.log("Client-side error occured.");
          } else {
            console.log("Server-side error occured.");
          }
          console.log(err);
          this.authentication.next(this.userInfo);
          observer.next(false);
        });
    });
  }
  */      
  

}
